﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathUnitTestingExercises
{
    [TestClass]
    public class TestMinElementinArrayClass
    {
        [TestMethod]
        public void TestMinElementInArrayGivenZeroLength()
        {
            //Arrange
            MathFunctions.MathFunctions math = new MathFunctions.MathFunctions();
            int[] testArr = { };
            //Act
            try
            {
                math.MinElementInArray(testArr);
            }
            //Assert
            catch
            {
                Assert.IsTrue(true);
            }
        }
        [TestMethod]
        public void TestMinElementInArrayGivenArray1()
        {
            //Arrange
            MathFunctions.MathFunctions math = new MathFunctions.MathFunctions();
            int[] testArr = { 1, 2, 3 };
            bool result;
            //Act
            if (math.MinElementInArray(testArr) == 1) result = true;
            else result = false;
            //Assert
            Assert.IsTrue(result);
        }
        [TestMethod]
        public void TestMinElementInArrayGivenArray2()
        {
            //Arrange
            MathFunctions.MathFunctions math = new MathFunctions.MathFunctions();
            int[] testArr = { 25, 35, 120 };
            bool result;
            //Act
            if (math.MinElementInArray(testArr) == 25) result = true;
            else result = false;
            //Assert
            Assert.IsTrue(result);
        }
    }
}
