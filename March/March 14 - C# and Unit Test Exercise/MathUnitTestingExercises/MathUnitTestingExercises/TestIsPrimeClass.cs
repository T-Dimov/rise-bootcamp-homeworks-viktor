﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathUnitTestingExercises
{
    [TestClass()]
    public class TestIsPrimeClass
    {
        [TestMethod]
        public void TestIsPrimeGivenZero()
        {
            //Arrange
            MathFunctions.MathFunctions math = new MathFunctions.MathFunctions();
            int testNum = 0;
            bool result;

            //Act
            result = math.IsPrime(testNum);

            //Assert
            Assert.IsFalse(result);
        }
        [TestMethod]
        public void TestIsPrimeGivenOne()
        {
            //Arrange
            MathFunctions.MathFunctions math = new MathFunctions.MathFunctions();
            int testNum = 1;
            bool result;

            //Act
            result = math.IsPrime(testNum);

            //Assert
            Assert.IsFalse(result);
        }
        [TestMethod]
        public void TestIsPrimeGivenPrimeNumber1()
        {
            //Arrange
            MathFunctions.MathFunctions math = new MathFunctions.MathFunctions();
            int testNum = 29;
            bool result;

            //Act
            result = math.IsPrime(testNum);

            //Assert
            Assert.IsTrue(result);
        }
        [TestMethod]
        public void TestIsPrimeGivenPrimeNumber2()
        {
            //Arrange
            MathFunctions.MathFunctions math = new MathFunctions.MathFunctions();
            int testNum = 29;
            bool result;

            //Act
            result = math.IsPrime(testNum);

            //Assert
            Assert.IsTrue(result);
        }
        [TestMethod]
        public void TestIsPrimeGivenNonPrimeNumber()
        {
            //Arrange
            MathFunctions.MathFunctions math = new MathFunctions.MathFunctions();
            int testNum = 6;
            bool result;

            //Act
            result = math.IsPrime(testNum);

            //Assert
            Assert.IsFalse(result);
        }
    }
}
